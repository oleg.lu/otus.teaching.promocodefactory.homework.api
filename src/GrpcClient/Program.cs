﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;

public class Program
{
    public static async Task Main()
    {
        using var channel = GrpcChannel.ForAddress("https://localhost:5001");

        var customerClient = new CustomServiceGrpc.CustomerService.CustomerServiceClient(channel);

        var response = customerClient.GetAllCustomers(new Empty());

        foreach (var customer in response.Customers)
            Console.WriteLine($"Customer ({customer.Id}): {customer.FirstName} {customer.LastName} ({customer.Email})");
        Console.ReadKey();
    }
}